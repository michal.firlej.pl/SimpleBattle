package com.example.michal.simplebattle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;

import android.widget.ImageView;
import android.widget.RadioButton;

import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int roundNumber;
    int leftPlayerHp = 5;
    int rightPlayerHp = 5;
    int leftPlayerDamage = 1;
    int rightPlayerDamage = 1;
    String leftPlayerName = "Lewy";
    String rightPlayerName = "Prawy";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public int randomDigit() {
        Random rn = new Random();
        int answer = rn.nextInt(10) + 1;
        displayRoundNumber(answer);
        return answer;
    }


    public void fight(View v) {


        roundNumber = roundNumber + 1;
        //displayRoundNumber(roundNumber);


        // Right player attack
        if (randomDigit() < 5) {


            leftPlayerHp = leftPlayerHp - rightPlayerDamage;
            displayRightPlayerActionInfo(leftPlayerHp);

            if (leftPlayerHp <= 0) {
                displayGameInfo("Right player win !! ");
                ImageView imageView1 = (ImageView) findViewById(R.id.playersAvatars);
                imageView1.setImageResource(R.drawable.right_player_win);
            }

            // Left player attack
        } else {


            rightPlayerHp = rightPlayerHp - leftPlayerDamage;
            displayLeftPlayerActionInfo(rightPlayerHp);

            if (rightPlayerHp <= 0) {
                displayGameInfo("Left player win!! ");
                ImageView imageView1 = (ImageView) findViewById(R.id.playersAvatars);
                imageView1.setImageResource(R.drawable.left_player_win);
            }


        }
    }

    private void displayRandomDigit(int i) {

        TextView random = (TextView) findViewById(R.id.game_info);
        random.setText("" + i);
    }

    private void displayRightPlayerActionInfo(int i) {

        TextView hpTextView = (TextView) findViewById(R.id.hp_left);
        hpTextView.setText("" + i);

        TextView rightPlayerAttack = (TextView) findViewById(R.id.game_info);
        rightPlayerAttack.setText(rightPlayerName + " zadaje " + rightPlayerDamage + " obrazen " + leftPlayerName + " !");

        ImageView imageView = (ImageView) findViewById(R.id.playersAvatars);
        imageView.setImageResource(R.drawable.right_player_attack);


    }

    private void displayLeftPlayerActionInfo(int i) {

        TextView hpTextView = (TextView) findViewById(R.id.hp_right);
        hpTextView.setText("" + i);

        TextView leftPlayerAttack = (TextView) findViewById(R.id.game_info);
        leftPlayerAttack.setText(leftPlayerName + " zadaje " + leftPlayerDamage + " obrażeń " + rightPlayerName + " !");

        ImageView imageView = (ImageView) findViewById(R.id.playersAvatars);
        imageView.setImageResource(R.drawable.left_player_attack);


    }


    public void displayGameInfo(String view) {
        TextView gameInfoTextView = (TextView) findViewById(R.id.game_info);
        gameInfoTextView.setText(" " + view);
    }

    public void displayRoundNumber(int i) {
        TextView roundNumberTextView = (TextView) findViewById(R.id.round_number);
        roundNumberTextView.setText("Runda nr " + i);
    }

    public void onRadioButtonClicked(View view) {

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked

        switch (view.getId()) {
            case R.id.super_attack_lewy:
                if (checked) leftPlayerDamage = 3;
                break;

            case R.id.pain_killer_lewy:
                if (checked) {
                    leftPlayerHp = leftPlayerHp + 5;
                    leftPlayerDamage = 1;
                    TextView statusLewego = (TextView) findViewById(R.id.game_info);
                    statusLewego.setText("Lewy odzyskal 5 punktow zycia.");
                }
                break;

            case R.id.basic_attack_lewy:
                if (checked) leftPlayerDamage = 1;
                break;

            case R.id.super_attack_prawy:
                if (checked) rightPlayerDamage = 3;
                break;

            case R.id.pain_killer_prawy:
                if (checked) {
                    rightPlayerHp = rightPlayerHp + 5;
                    rightPlayerDamage = 1;
                    TextView statusPrawego = (TextView) findViewById(R.id.game_info);
                    statusPrawego.setText("Prawy odzyskal 5 punktow zycia.");
                }
                break;

            case R.id.basic_attack_prawy:
                if (checked) rightPlayerDamage = 1;
        }

    }


}



